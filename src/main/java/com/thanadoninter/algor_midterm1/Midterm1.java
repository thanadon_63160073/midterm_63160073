/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.algor_midterm1;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class Midterm1 {
    public static void main(String[] args) throws FileNotFoundException {
            Scanner kb = new Scanner(System.in);
            String text = kb.nextLine();
            String[] arr = text.split(" ");
            
            System.out.print("inut: "+ Arrays.toString(arr)+" ---> ");
            arr = swap(arr);
            System.out.println("output: "+ Arrays.toString(arr));
    }

    public static String[] swap(String[] list){
        String n;
        for(int i = 0, j = list.length-1; i < j; i++, j--){
            n = list[i];
            list[i] = list[j];
            list[j] = n;
        }
        return list;
    }
}
