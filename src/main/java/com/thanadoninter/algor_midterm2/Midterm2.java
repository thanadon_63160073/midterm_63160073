/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.algor_midterm2;

import java.io.FileNotFoundException;
import static java.lang.Integer.parseInt;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class Midterm2 {

    public static int[] sorted_Subarray(int[] arr) {
        int maxLen = 1, len = 1, startIndex = 0;
        for (int i = 0; i < arr.length-1; i++) {
            if (arr[i] < arr[i + 1]) {
                len++;
            } else {
                if (maxLen < len) {
                    maxLen = len;
                    startIndex = i+1 - maxLen;
                }
                len = 1;
            }
        }
        if (maxLen < len)
        {
            maxLen = len;
            startIndex = arr.length - maxLen;
        }
        
        int[] subarray = new int[maxLen];
        for (int i = startIndex; i < maxLen + startIndex; i++) {
            subarray[i - startIndex] = arr[i];
        }
        return subarray;
    }
    
    public static void main(String[] args) throws FileNotFoundException {
            Scanner kb = new Scanner(System.in);
            String text = kb.nextLine();
            String[] arr = text.split(" ");
            
            int[] num = new int[arr.length];
            for(int j = 0; j < num.length; j++){
                int n = parseInt(arr[j]);
                num[j] = n;
            }
            
            System.out.print("input: "+ Arrays.toString(num)+" ---> ");
            num = sorted_Subarray(num);
            System.out.println("output: "+ Arrays.toString(num));
    }
}
